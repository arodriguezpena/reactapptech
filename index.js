/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import router from './router'
AppRegistry.registerComponent(appName, () => router);
