import { createStackNavigator, createAppContainer } from "react-navigation";
import App from './App'
import List from './List'
const Router = createStackNavigator({
  Login: {
    screen: App,
    navigationOptions: {
      header: null // Will hide header for HomePage
    }
  },
  List: {
    screen: List,
  }
});

export default createAppContainer(Router);