/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/emin93/react-native-template-typescript
 *
 * @format
 */

import React, {Fragment} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  TextInput,
  TouchableHighlight,
  View,
  ImageBackground,
  Text,
  StatusBar,
  Alert,
} from 'react-native';

import TechConnectionReact from 'tech-connection-react'
 
class App extends React.Component {
  state = {
    email: 'eve.holt@reqres.in',
    password: '123123',
    loading: false
  }

  submit() {
    let {email, password} = this.state
    this.setState({loading: true})
    TechConnectionReact.login(email,password).then(data => {
      if(data.success) {
        this.props.navigation.navigate('List')
      } else {
        Alert.alert("Technisys", data.message!)
      }
      this.setState({loading: false})
    })
  }
  render() {
    let {email, password, loading} = this.state
    return (
      <ImageBackground blurRadius={2}  source={require('./assets/city.jpg')} style={{width: '100%', height: '100%'}}>
        <StatusBar barStyle="light-content"/>
        <SafeAreaView style={styles.container}>
          <View style={styles.view}>
            <Text style={styles.h1}>Bienvenido</Text>
            <TextInput 
              onChangeText={(email) => this.setState({email})}
              value={email}
              style={styles.input} 

              placeholderTextColor="#ecf0f1"
              placeholder="Escriba su email"></TextInput>
            <TextInput 
            onChangeText={(password) => this.setState({password})}
            value={password}
              style={styles.input} 
              placeholderTextColor="#ecf0f1"
              placeholder="Escriba su contraseña"></TextInput>
            <TouchableHighlight
              style={styles.button}
              onPress={this.submit.bind(this)}>
                <View>
                {!loading &&
                   <Text style={styles.textButton}>Login</Text>
                }
                {loading &&
                   <Text style={styles.textButton}>Espere un momento...</Text>
                }
                </View>
            </TouchableHighlight>
          </View>
        </SafeAreaView>
      </ImageBackground>
    )
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginLeft:40,
    marginRight: 40
  },
  view: {
    marginTop: 180
  },
  h1: {
    color: 'white',
    fontSize: 30,
    fontWeight: 'bold',
    textShadowColor: 'rgba(0, 0, 0, 0.75)',
    textShadowOffset: {width: -1, height: 1},
    textShadowRadius: 10
  },
  input: {
    color: 'white',
    height: 40,
    borderBottomWidth: 2,
    borderBottomColor: '#ecf0f1',
    marginTop: 5,
    marginBottom: 5,
    textShadowColor: 'rgba(0, 0, 0, 0.75)',
    textShadowOffset: {width: -1, height: 1},
    textShadowRadius: 10,
    fontSize: 14,
    fontWeight: 'bold'


  }, 
  button: {
    marginTop: 20,
    height: 50,
    backgroundColor: '#ecf0f1',
    borderRadius: 40,
    alignItems: 'center',
    justifyContent: 'center',

  },
  textButton: {
    color: 'grey',
    fontWeight: 'bold'
  }
})

export default App;