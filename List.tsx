import React, { Component } from 'react'
import { Text, View, Alert, ActivityIndicator, StyleSheet, FlatList, Image } from 'react-native'
import TechConnectionReact from 'tech-connection-react'
interface User {
  id: number;
  email: string;
  first_name: string;
  last_name:string;
  avatar: string;
}
export default class List extends Component {
  static navigationOptions = {
    title: 'Listado de usuarios',
  };
  state = {
    loading: true,
    users: []
  }
  componentDidMount() {
    TechConnectionReact.getUsers(0).then(data => {
      if (data.success) {
        this.setState({users: data.data!})
        console.log("xxxx",data.data!)
      } else {
        Alert.alert("Technisys", data.message!)
      }
      this.setState({loading: false})
    })
  }
  render() {
    let {loading} = this.state
    let users = this.state.users as User[]
    if (loading) {
      return (
        <View style={styles.loading}>
          <ActivityIndicator></ActivityIndicator>
          <Text style={styles.loadingText}>Espere un momento</Text>
        </View>
      )
    }
    return (
      <View style={styles.container}>
        <FlatList
        data={users}
        renderItem={this.renderItem.bind(this)}/>
      </View>
    )
  }
  renderItem({item}) {
    let user = item as User
    return (
      <View style={styles.row}>
        <View style={{flex:2}}>
          <Image 
            style={styles.avatar}
            source={{uri: user.avatar}}></Image>
        </View>
        <View style={{flex:8, paddingTop: 5}}>
          <Text style={{fontWeight:'bold'}}>{user.first_name} {user.last_name}</Text>   
          <Text>{user.email}</Text>   
        </View>
      </View>
    )
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ecf0f1'
  },
  loading: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  loadingText: {
    marginTop: 10,
    color: '#7f8c8d'
  },
  row: {
    paddingLeft: 10,
    paddingTop: 10,
    flexDirection: 'row',
    height: 70,
    backgroundColor: 'white',
    marginBottom: 2,
    justifyContent: 'center'
  },
  avatar: {
    width: 50, 
    height: 50,
    borderRadius: 10
  }
})
